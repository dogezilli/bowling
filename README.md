## Bowling Challenge

### Requirements

- Java (JDK & JRE 8)
- Apache Maven

### Run application

Run on the terminal at the root directory of this project:

`$ mvn clean install && mvn exec:java -Dexec.args="game.txt"`

### Notes

One commit before "790fe563 - All values are tab separated", the printBoard method has a more visible table than the requisit "b. All values are tab-separated."