package utils;

import org.junit.Test;
import utils.Bowling;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class BowlingTest {

    @Test
    public void isSpare() {
        Bowling bowling = new Bowling();
        assertEquals("Is Spare", true, bowling.isSpare(4, 6));
    }

    @Test
    public void isNotSpare() {
        Bowling bowling = new Bowling();
        assertEquals("Is not Spare", false, bowling.isSpare(4, 4));
    }

    @Test
    public void isStrike() {
        Bowling bowling = new Bowling();
        assertEquals("Is Strike", true, bowling.isStrike(10));
    }

    @Test
    public void isNotStrike() {
        Bowling bowling = new Bowling();
        assertEquals("Is not Strike", false, bowling.isStrike(9));
    }

    @Test
    public void hasNextRound() {
        Bowling bowling = new Bowling();
        int[] rounds = {9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,0};
        assertEquals("Is not the end of the game", false, bowling.hasNextRound(19, rounds.length));
    }

    @Test
    public void isLastRound() {
        Bowling bowling = new Bowling();
        assertEquals("Is the end of the game", false, bowling.hasNextRound(9, 10));
    }

    @Test
    public void validGame90Points() {
        Bowling bowling = new Bowling();
        int[] game = {9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,0};
        List<Integer> total = bowling.getScore(game).getTotal();
        int points = total.get(total.size() - 1);
        assertEquals("Total points", 90, points);
    }

    @Test
    public void validGame150Points() {
        Bowling bowling = new Bowling();
        int[] game = {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
        List<Integer> total = bowling.getScore(game).getTotal();
        int points = total.get(total.size() - 1);
        assertEquals("Total points", 150, points);
    }

    @Test
    public void validGame155Points() {
        Bowling bowling = new Bowling();
        int[] game = {10,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
        List<Integer> total = bowling.getScore(game).getTotal();
        int points = total.get(total.size() - 1);
        assertEquals("Total points", 155, points);
    }


    @Test
    public void validPerfectGame() {
        Bowling bowling = new Bowling();
        int[] game = {10,10,10,10,10,10,10,10,10,10,10,10};
        List<Integer> total = bowling.getScore(game).getTotal();
        int points = total.get(total.size() - 1);
        assertEquals("Total points", 300, points);
    }
}
