import model.Game;
import utils.BoardPrinter;
import utils.FileReader;
import utils.GameBuilder;

import java.util.stream.Stream;

public class App {

    public static void main(String args[]) {
        FileReader fileReader = new FileReader();
        GameBuilder gameBuilder = new GameBuilder();
        BoardPrinter boardPrinter = new BoardPrinter();
        if(args.length==0){
            System.err.println("No file found. Please run the command with filename, ie:\nmvn clean install && mvn exec:java -Dexec.args=\"game.txt\"");
            return;
        }
        Stream<String> lines = fileReader.readFile(args[0]);
        Game game = gameBuilder.buildGame(lines);
        boardPrinter.printBoard(game);
    }
}
