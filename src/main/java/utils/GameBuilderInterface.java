package utils;

import model.Game;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public interface GameBuilderInterface {
    Game buildGame(Stream<String> lines);
    Game setGameValues(List<String> players, List<HashMap<String, String>> rounds);
}
