package utils;

import model.Game;
import model.Score;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public class GameBuilder implements GameBuilderInterface {

    @Override
    public Game buildGame(Stream<String> lines) {
        List<HashMap<String, String>> rounds = new ArrayList<>();
        List<String> players = new ArrayList<>();
        lines.forEach(line -> {
            String player = line.substring(0, line.indexOf(" "));
            String points = line.substring(line.indexOf(" ") + 1);
            HashMap<String, String> round = new HashMap();
            round.put(player, points);
            rounds.add(round);

            if (!players.contains(player)) {
                players.add(player);
            }
        });

        return this.setGameValues(players, rounds);
    }

    @Override
    public Game setGameValues(List<String> players, List<HashMap<String, String>> rounds) {
        Bowling bowling = new Bowling();
        Game game = new Game();
        List<Score> scores = new ArrayList<>();
        List<int[]> gameIntValues = new ArrayList<>();

        players.forEach(player -> {
            List<Integer> intValues = new ArrayList<>();
            rounds.forEach(round -> {
                for(HashMap.Entry<String, String> item : round.entrySet()) {
                    String key = item.getKey();
                    String value = item.getValue();

                    if (key.equals(player)) {
                        if (value.equals("F")) {
                            intValues.add(0);
                        } else {
                            intValues.add(Integer.parseInt(value));
                        }
                    }
                }
            });
            int[] values = intValues.stream().mapToInt(i->i).toArray();
            Score score = bowling.getScore(values);
            scores.add(score);
            gameIntValues.add(values);
        });

        game.setPlayers(players);
        game.setScore(scores);
        return game;
    }
}
