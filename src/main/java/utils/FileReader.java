package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileReader implements FileReaderInterface {

    @Override
    public Stream<String> readFile(String file) {
        Stream<String> lines = Stream.empty();
        try {
            lines = Files.lines(Paths.get(file));
        }catch (IOException e){
            System.out.println("File not found.");
        }
        return lines;
    }
}