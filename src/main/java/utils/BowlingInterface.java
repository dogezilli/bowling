package utils;

import model.Score;

public interface BowlingInterface {
    Score getScore(int[] rounds);
    boolean isSpare(int actual, int next);
    boolean isStrike(int actual);
    boolean hasNextRound(int position, int size);
}
