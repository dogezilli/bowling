package utils;

import model.Score;

import java.util.ArrayList;
import java.util.List;

public class Bowling implements BowlingInterface {

    @Override
    public Score getScore(int[] rounds) {
        Score score = new Score();
        List<String> results = new ArrayList<>();
        List<Integer> total = new ArrayList<>();
        int points = 0;
        int i = 0;

        if (rounds.length < 12 || rounds.length > 21) {
            return score;
        }

        do {
            if (isStrike(rounds[i])) {
                results.add("X");
                points += rounds[i];
                points += rounds[i + 1];
                points += rounds[i + 2];
                i++;
            } else if (isSpare(rounds[i], rounds[i + 1])) {
                results.add(String.valueOf(rounds[i]));
                results.add("/");
                points += rounds[i];
                points += rounds[i + 1];
                points += rounds[i + 2];
                i += 2;
            } else {
                results.add(String.valueOf(rounds[i]));
                results.add(String.valueOf(rounds[i + 1]));
                points += rounds[i];
                points += rounds[i + 1];
                i += 2;
            }
            total.add(points);
        } while (hasNextRound(i, rounds.length));

        extraChances(i, rounds, results);
        score.setValues(results);
        score.setTotal(total);
        return score;
    }

    @Override
    public boolean isSpare(int actual, int next) {
        return (actual + next) == 10;
    }

    @Override
    public boolean isStrike(int actual) {
        return actual == 10;
    }

    @Override
    public boolean hasNextRound(int position, int size) {
        return ((position + 2) < size);
    }

    private void extraChances(int i, int[] rounds, List<String> results) {
        if (i < rounds.length) {
            if (isStrike(rounds[i])) {
                results.add("X");
            } else {
                results.add(String.valueOf(rounds[i]));
            }
        }
        if ((i + 1) < rounds.length) {
            if (isStrike(rounds[i + 1])) {
                results.add("X");
            } else if (isSpare(rounds[i], rounds[i + 1])) {
                results.add("/");
            } else {
                results.add(String.valueOf(rounds[i + 1]));
            }
        }
    }
}
