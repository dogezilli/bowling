package utils;

import java.util.stream.Stream;

public interface FileReaderInterface {
    Stream<String> readFile(String file);
}
