package utils;

import model.Game;

import java.util.List;
import java.util.stream.IntStream;

public class BoardPrinter implements BoardPrinterInterface {

    private String buildFrame() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Frame");
        IntStream.range(1, 11).forEach(e -> {
            stringBuilder.append("\t");
            stringBuilder.append(e);
        });
        return stringBuilder.toString();
    }

    private String buildPinfalls(List<String> values) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Pinfalls");
        values.forEach(value -> {
            stringBuilder.append("\t");
            stringBuilder.append(value);
        });

        return stringBuilder.toString();

    }

    private String buildScore(List<Integer> points) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Score");
        points.forEach(point -> {
            stringBuilder.append("\t");
            stringBuilder.append(point);
        });
        return stringBuilder.toString();
    }

    @Override
    public String buildBoard(Game game) {
        int size = game.getPlayers().size();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(buildFrame());
        stringBuilder.append("\n");
        for (int i = 0; i < size; i++) {
            stringBuilder.append(game.getPlayers().get(i));
            stringBuilder.append("\n");
            stringBuilder.append(buildPinfalls(game.getScore().get(i).getValues()));
            stringBuilder.append("\n");
            stringBuilder.append(buildScore(game.getScore().get(i).getTotal()));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public void printBoard(Game game) {
        System.out.println(buildBoard(game));
    }
}
