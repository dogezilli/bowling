package utils;

import model.Game;

public interface BoardPrinterInterface {
    String buildBoard(Game game);
    void printBoard(Game game);
}